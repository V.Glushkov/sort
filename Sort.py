

arr = [4, 1, 88, 32, 5, 9]

for i in range (0, len(arr)-1):
    min = i
    for j in range (i + 1, len(arr)):
        if(arr[j] < arr[min]):
            min = j
    temp = arr[i]
    arr[i] = arr[min]
    arr[min] = temp

print(arr)

